# LoRaWAN-Beaulieu-Measurement-2021

This GitLab repository is providing access to the research community to the LoRawan data gathered in Rennes in 2021.

It is an additional information for supporting the results presented in the following paper :

"Decentralized Adaptive Spectrum Learning in Wireless IoT Networks based on Channel Quality Information"
